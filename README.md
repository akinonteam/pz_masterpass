

# pz_masterpass

Library package for enabling MasterPass payment option on Checkout pages.


## Installation

 - At project root, create a requirements.txt if it doesn't exist and add the following line to it.

	   -e git+https://git@bitbucket.org/akinonteam/pz_masterpass.git@ad41c6f#egg=pz_masterpass

- Run following command to install the package:

	  pip install -r requirements.txt

- Install the pz-masterpass package:

      yarn add git+ssh://git@bitbucket.org:akinonteam/pz_masterpass.git#ad41c6f

 - Install the related shop-packages version or higher:

       yarn add git+ssh://git@bitbucket.org:akinonteam/shop-packages.git#3794524

- Add following line to omnife_base/settings.py:

        INSTALLED_APPS.append('pz_masterpass')

 - Add the following lines to layout/scripts.html:

		<script src="{{ static('js/mfs-client.min.js') }}"></script>
		<script>
			MFS.setClientId('{{ masterpassjs().client_id }}');
			MFS.setAddress("{{ masterpassjs().js }}");
		</script>

## Usage

 - Add following line to checkout/index.html:
 
       {% include 'pz_masterpass/index.html' %}

 - Add following line to checkout/index.scss:

       @import '~pz-masterpass/';

- Change credit_card payment type to masterpass in related files.

	**checkout/payment/index.html:**
	
	  ...
	  <button class="checkout-payment-type__button js-payment-tab" data-type="masterpass" hidden>
	  ...
	  ...
	  <div class="js-payment-tab-content" data-type="masterpass" hidden>
	  ...

	**checkout/payment/index.js:**
	
	  paymentOptions = new  Map(
	  [['masterpass', CreditCardOption], ['funds_transfer', FundsTransferOption], ['gpay', GPayOption]]
      );

	  currentPaymentOption = 'masterpass';
	 
- Wrap your credit card form content with **js-checkout-cc-form-wrapper** and add MasterPass card list wrapper. 

	**checkout/payment/credit-card/index.html**

		<div class="checkout-payment-type__content">
			<!-- ADD FOLLOWING WRAPPER -->
			<div class="js-checkout-cc-form-wrapper">
				<div class="checkout-payment-type__description">
					...
				</div>
				<div class="checkout-credit-card__form">
					...
				</div>
			</div>

			<!-- ADD FOLLOWING LINE -->
			<div class="pz-mp-card-list-wrapper js-mp-card-list-wrapper"  hidden></div>
		</div>
		...

- Use setMasterPassInstallment action

	**checkout/payment/credit-card/index.js:**

		import { setBinNumber, setInstallment, setMasterPassInstallment } from 'shop-packages/redux/checkout/actions';

		...
		...

		renderInstallments = () => {
			...
			...
			
			const  currentPaymentOption = getValue(selectCurrentPaymentOption);

			element.addEventListener('input', (e) =>
			store.dispatch(
				(currentPaymentOption.payment_type === 'masterpass'
					? setMasterPassInstallment : setInstallment)(e.currentTarget.value)
			));
		}

- Import and use MasterPass

	**checkout/index.js:**

		import MasterPass from 'pz-masterpass';

		...

		constructor() {
			...
	
			new  MasterPass({
				validateCreditCardForm: () => {
					const  form = document.querySelector('.js-credit-card-form');

					return  form && form.validate();
				},
				validateAgreementCheckbox: () => {
					const  input = document.querySelector('.js-credit-card-form .js-checkout-agreement-input');

					return  input && input.validate();
				}
			});
		}

## Options

| Name | Default |
|--|--|
| cardFormWrapperSelector | '.js-checkout-cc-form-wrapper' |
| cardHolderSelector | '.js-checkout-card-holder' |
| cardNumberSelector | '.js-checkout-card-number' |
| cardExpirationMonthSelector | '.js-checkout-card-month' |
| cardExpirationYearSelector | '.js-checkout-card-year' |
| cardCvvSelector | '.js-checkout-card-cvv' |
| creditCardFormSelector | '.js-credit-card-form' |
| cardListWrapperSelector | '.js-mp-card-list-wrapper' |
| deleteIconClassName | 'pz-icon-close' |
| installmentsEmptyViewSelector | '.js-checkout-installments-empty-view' |
| installmentsContainerSelector | '.js-checkout-installments-container' |
| installmentRadioSelector | '.js-installment-input' |
| checkoutErrorSelector | '.js-agreement-error' |
| otpTimeout | 120000 |
| language | eng |
| translation | Check out the "Translation" section |

### Translation

You can pass **translation** object with following properties.

	{
		youHaveRegisteredCards:  gettext('You have cards registered to MasterPass.'),
		linkCards:  gettext('You have cards registered to your MasterPass account. Would you like to use your cards?'),
		payWithNewCard:  gettext('Pay with new credit card.'),
		payWithMyMasterPassCard:  gettext('I want to pay with my card registered to MasterPass.'),
		agreementText:  gettext('I consent to the registration of my credit card information with MasterPass.'),
		masterPassCardName:  gettext('MasterPass Card Name'),
		pleaseSelectInstallment:  gettext('Please select an installment'),
		pleaseSelectCard:  gettext('Please select a card'),
		pleaseEnterCardName:  gettext('Please enter card name.'),
		save:  gettext('Save'),
		confirm:  gettext('Confirm')
	}