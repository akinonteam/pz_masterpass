/* eslint-disable no-console */
import { observe, store } from 'shop-packages';
import { setMasterPassBinNumber, getMasterPassOrderNo, completeMasterPassOrder } from 'shop-packages/redux/checkout/actions';
import { setPending } from 'shop-packages/redux/checkout/reducer';
import {
  selectCurrentPaymentOption,
  selectPreOrder,
  selectTab,
  selectMasterPassOrder,
  selectSummaryData,
  selectInstallments,
  selectCurrentInstallmentId,
  selectBinNumberLength
} from 'shop-packages/redux/checkout/selectors';
import { getValue } from 'shop-packages/redux/connectSelector';

if (!window.gettext) {
  window.gettext = (str) => str;
}

export default class MasterPass {
  token;

  referenceNo;

  initialized = false;

  cardFormWrapperSelector;

  cardHolderSelector;

  cardNumberSelector;

  cardExpirationMonthSelector;

  cardExpirationYearSelector;

  cardCvvSelector;

  creditCardFormSelector;

  cardListWrapperSelector;

  deleteIconClassName;

  otpModalIconClassName;

  infoIconClassName;

  infoText;

  installments = [];

  installmentsEmptyViewSelector;

  installmentsContainerSelector;

  installmentRadioSelector;

  checkoutErrorSelector;

  onError;

  onLinkAccountSuccess;

  validateCreditCardForm;

  validateAgreementCheckbox;

  otpTimeout;

  countdownInterval;

  isDirectPurchase = true;

  isPaymentReady = false;

  language;

  translation;

  responseMessages;

  binNumber;

  additionalParams;

  constructor({
    cardFormWrapperSelector = '.js-checkout-cc-form-wrapper',
    cardHolderSelector = '.js-checkout-card-holder',
    cardNumberSelector = '.js-checkout-card-number',
    cardExpirationMonthSelector = '.js-checkout-card-month',
    cardExpirationYearSelector = '.js-checkout-card-year',
    cardCvvSelector = '.js-checkout-card-cvv',
    creditCardFormSelector = '.js-credit-card-form',
    cardListWrapperSelector = '.js-mp-card-list-wrapper',
    deleteIconClassName = 'pz-icon-close',
    otpModalIconClassName = 'pz-icon-mp-otp',
    infoIconClassName = null,
    infoText = '',
    cardImageContainerSelector = '.js-checkout-card-image-container',
    installmentsEmptyViewSelector = '.js-checkout-installments-empty-view',
    installmentsContainerSelector = '.js-checkout-installments-container',
    installmentRadioSelector = '.js-installment-input',
    checkoutErrorSelector = '.js-agreement-error',
    otpTimeout = 120000,
    language = null,
    translation = null,
    responseMessages,
    onError = () => {},
    onLinkAccountSuccess = () => {},
    validateCreditCardForm = () => true,
    validateAgreementCheckbox = () => true,
    additionalParams = null
  } = {}) {
    const defualtTranslations = {
      youHaveRegisteredCards: gettext('You have cards registered to MasterPass.'),
      linkCards: gettext('You have cards registered to your MasterPass account. Would you like to use your cards?'),
      payWithNewCard: gettext('Pay with new credit card.'),
      payWithMyMasterPassCard: gettext('I want to pay with my card registered to MasterPass.'),
      agreementText: gettext('I consent to the registration of my credit card information with MasterPass.'),
      masterPassCardName: gettext('MasterPass Card Name'),
      pleaseSelectInstallment: gettext('Please select an installment'),
      pleaseSelectCard: gettext('Please select a card'),
      pleaseEnterCardName: gettext('Please enter card name.'),
      deleteCardConfirmation: gettext('Are you sure you want to delete this card?'),
      threeDTransactionFailed: gettext('3D payment transaction failed.'),
      resendSms: gettext('Send SMS Again'),
      confirmationCode: gettext('Confirmation Code'),
      delete: gettext('Delete'),
      save: gettext('Save'),
      use: gettext('Use'),
      confirm: gettext('Confirm'),
      merchantVerification: gettext('Please enter otp for merchant verification')
    };

    this.cardFormWrapperSelector = cardFormWrapperSelector;
    this.cardHolderSelector = cardHolderSelector;
    this.cardNumberSelector = cardNumberSelector;
    this.cardExpirationMonthSelector = cardExpirationMonthSelector;
    this.cardExpirationYearSelector = cardExpirationYearSelector;
    this.cardCvvSelector = cardCvvSelector;
    this.creditCardFormSelector = creditCardFormSelector;
    this.cardListWrapperSelector = cardListWrapperSelector;
    this.deleteIconClassName = deleteIconClassName;
    this.otpModalIconClassName = otpModalIconClassName;
    this.infoIconClassName = infoIconClassName;
    this.infoText = infoText;
    this.cardImageContainerSelector = cardImageContainerSelector;
    this.installmentsContainerSelector = installmentsContainerSelector;
    this.installmentsEmptyViewSelector = installmentsEmptyViewSelector;
    this.installmentRadioSelector = installmentRadioSelector;
    this.checkoutErrorSelector = checkoutErrorSelector;
    this.otpTimeout = otpTimeout;
    this.language = language;
    this.translation = { ...defualtTranslations, ...(translation || {}) };
    this.responseMessages = responseMessages;
    this.onError = onError;
    this.onLinkAccountSuccess = onLinkAccountSuccess;
    this.validateCreditCardForm = validateCreditCardForm;
    this.validateAgreementCheckbox = validateAgreementCheckbox;
    this.additionalParams = additionalParams;

    observe(selectTab).subscribe(this.onCheckoutTabChange);
    observe(selectCurrentPaymentOption).subscribe(this.onPaymentOptionChange);
    observe(selectMasterPassOrder).subscribe(this.onMasterPassOrderUpdate);
    observe(selectInstallments).subscribe(this.onInstallmentsUpdate);
  }

  init = async () => {
    new MutationObserver((mutationsList, observer) => {
      this.toggleInstallmentsVisibility();
    }).observe(document.querySelector(this.installmentsContainerSelector), {
      attributes: true,
      childList: false,
      subtree: true
    });

    document.querySelector(this.cardListWrapperSelector).innerHTML = `
      <p class="pz-mp-card-list-info">${this.translation.youHaveRegisteredCards}</p>
      <ul class="pz-mp-card-list js-mp-card-list"></ul>
      <a href="#" class="pz-mp-switch-payment-type-btn js-mp-switch-payment-type" data-type="direct">
        ${this.translation.payWithNewCard}
      </a>
    `;

    if (!document.querySelector('.js-mp-direct-purchase')) {
      document.querySelector(this.cardFormWrapperSelector).insertAdjacentHTML(
        'beforeend',
        `
        <div class="pz-mp-direct-purchase js-mp-direct-purchase">
          <div class="pz-mp-save-card">
            <div class="pz-mp-save-card-cb-w">
              <span class="pz-mp-save-card-cb-w__checkbox js-mp-save-card-checkbox"></span>
              <p class="pz-mp-save-card-cb-w__text">${this.translation.agreementText}</p>
              <img src="${window.URLS.static}img/mp_masterpass-logo.png" class="pz-mp-save-card-cb-w__logo" />
              ${this.infoIconClassName ? `<i class="${this.infoIconClassName} pz-mp-info-icon js-mp-info-btn"></i>` : ''}
            </div>
            <div class="pz-mp-save-card__form-w js-mp-save-card-form-wrapper" hidden>
              <label class="pz-mp-save-card__label">${this.translation.masterPassCardName}</label>
              <input type="text" class="pz-mp-input js-mp-save-card-input" />
              <p class="pz-mp-save-card__error js-mp-save-card-error" hidden></p>
              <button class="pz-mp-save-card__button pz-mp-button p js-mp-save-card-button">
                ${this.translation.save}
              </button>
            </div>
          </div>
        </div>
      `
      );
    }

    await this.requestToken();

    if (!this.token) {
      return;
    }

    this.checkPaymentErrors();
    this.checkUser();
    this.bindEvents();
    this.initialized = true;
  };

  bindEvents = () => {
    document.querySelector('.js-mp-save-card-button').removeEventListener('click', this.registerCard);
    document.querySelector('.js-mp-save-card-button').addEventListener('click', this.registerCard);
    document.querySelector(this.creditCardFormSelector).removeEventListener('submit', this.onCreditCardFormSubmit);
    document.querySelector(this.creditCardFormSelector).addEventListener('submit', this.onCreditCardFormSubmit);
    ['keyup', 'paste'].forEach((eventName) => {
      document.querySelector(this.cardNumberSelector).removeEventListener(eventName, this.onCreditCardNumberChange);
      document.querySelector(this.cardNumberSelector).addEventListener(eventName, this.onCreditCardNumberChange);
    });
    document.querySelector('.js-mp-save-card-checkbox').removeEventListener('click', this.onSaveCardCheckboxClick);
    document.querySelector('.js-mp-save-card-checkbox').addEventListener('click', this.onSaveCardCheckboxClick);
    document.querySelector('.js-mp-info-btn').addEventListener('click', () => {
      this.showModal({
        contentHTML: this.infoText,
        showLogo: true
      });
    });
    this.bindSwitchButtonEvents();
  };

  linkCardToClient = ({ form, message = null }) => {
    MFS.linkCardToClient($(form), async (statusCode, response) => {
      if (response.responseCode == '5001' || response.responseCode == '5008') {
        const { statusCode: statusCode_, response: response_ } = await this.showOtpModal({
          title: message || this.responseMessages?.[response.responseCode] || response.responseDescription,
          message
        });

        statusCode = statusCode_;
        response = response_;
      }

      if (response.responseCode == '0000' || response.responseCode == '3838' || response.responseCode == '') {
        this.onLinkAccountSuccess();
        this.listCards();
      }

      this.closeModal();
    });
  };

  onSaveCardCheckboxClick = (e) => {
    const cb = e.currentTarget;

    cb.classList.toggle('-checked');
    document.querySelector('.js-mp-save-card-form-wrapper').toggleAttribute('hidden', !cb.classList.contains('-checked'));
  };

  bindSwitchButtonEvents = () => {
    document.querySelectorAll('.js-mp-switch-payment-type').forEach((button) => {
      button.removeEventListener('click', this.onSwitchPaymentTypeButtonClick);
      button.addEventListener('click', this.onSwitchPaymentTypeButtonClick);
    });
  };

  fillPurchaseFormCommonFields = (form, order) => {
    const installments = getValue(selectInstallments);
    const currentInstallmentId = getValue(selectCurrentInstallmentId);
    const installmentCount = parseInt(installments.find((inst) => inst.pk === currentInstallmentId)?.installment_count);
    const { totalAmount, totalAmountWithInterest } = getValue(selectSummaryData);

    form.querySelector('[name="token"]').value = order.token;
    form.querySelector('[name="orderNo"]').value = order.order_no;
    form.querySelector('[name="referenceNo"]').value = this.referenceNo;
    form.querySelector('[name="amount"]').value = (totalAmountWithInterest || totalAmount).replace('.', '');
    form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();

    if (!isNaN(installmentCount)) {
      form.querySelector('[name="installmentCount"]').value = installmentCount;
    }
  };

  getDirectPurchaseForm = (order) => {
    const template = document.querySelector('.js-mp-direct-purchase-form-template');
    const form = template.content.querySelector('form');
    const cardHolder = document.querySelector(this.cardHolderSelector)?.value || '';

    this.fillFormCreditCardFields(form);
    this.fillPurchaseFormCommonFields(form, order);
    form.querySelector('[name="cardHolderName"]').value = cardHolder;

    return $(form);
  };

  getPurchaseForm = (order) => {
    const template = document.querySelector('.js-mp-purchase-form-template');
    const form = template.content.querySelector('form');

    this.fillPurchaseFormCommonFields(form, order);

    return $(form);
  };

  onMasterPassOrderUpdate = (order) => {
    if (!order || !this.isPaymentReady) {
      return;
    }

    let additionalParams = null;

    if (order.extras?.additionalParams) {
      additionalParams = order.extras.additionalParams;
    }

    if (this.additionalParams) {
      additionalParams = this.additionalParams;
    }

    if (additionalParams) {
      MFS.setAdditionalParameters(additionalParams);
    }

    this.isPaymentReady = false;

    MFS[this.isDirectPurchase ? 'directPurchase' : 'purchase'](
      this.isDirectPurchase ? this.getDirectPurchaseForm(order) : this.getPurchaseForm(order),
      async (statusCode, response) => {
        if (response.responseCode == '5010') {
          const return3dUrl = document.querySelector('.js-mp-return-3d-url').value;

          this.handle3DRedirection(response.url3D + '&returnUrl=' + encodeURIComponent(return3dUrl));
          return;
        }

        if (response.responseCode == '5001') {
          const { statusCode: statusCode_, response: response_ } = await this.showOtpModal({
            title: this.responseMessages?.[response.responseCode] || response.responseDescription
          });

          statusCode = statusCode_;
          response = response_;
        }

        if (response.token && (response.responseCode == '0000' || response.responseCode == '')) {
          store.dispatch(completeMasterPassOrder(response.token));
          return;
        }

        if (response.responseDescription.length) {
          this.showModal({
            contentHTML: this.responseMessages?.[response.responseCode] || response.responseDescription
          });
        }

        store.dispatch(setPending(false));
      }
    );
  };

  handle3DRedirection = (redirectUrl) => {
    const isMobileApp = false; // localStorage.getItem('isMobileApp'); // disable iframe due to ios 18 problems
    const triggerMobile3DEvent = localStorage.getItem('triggerMobile3DEvent');

    if (isMobileApp) {
      if (triggerMobile3DEvent) {
        document.dispatchEvent(
          new CustomEvent('open-3d-iframe', {
            detail: { redirectUrl }
          })
        );
      } else {
        const iframeWrapper = document.createElement('div');
        const iframe = document.createElement('iframe');
        const closeButton = document.createElement('div');

        const iframeURLChange = (iframe, callback) => {
          iframe.addEventListener('load', () => {
            setTimeout(() => {
              if (iframe?.contentWindow?.location) {
                callback(iframe.contentWindow.location);
              }
            }, 0);
          });
        };

        const removeIframe = async () => {
          const iframeSelector = document.querySelector('.js-checkout-payment-iframe-wrapper');

          if (!iframeSelector) {
            return;
          }

          iframeSelector.remove();

          const iframeLoading = document.createElement('div');

          iframeLoading.className = 'iframe-loading-wrapper js-iframe-loading-wrapper';
          document.body.appendChild(iframeLoading);

          await fetch(`${window.URLS.checkout}`, {
            method: 'POST',
            headers: {
              Accept: 'application/json'
            }
          });

          iframeLoading.remove();
        };

        iframeWrapper.className = 'checkout-payment-iframe-wrapper js-checkout-payment-iframe-wrapper';
        closeButton.className = 'checkout-payment-iframe-wrapper-close-button js-checkout-payment-iframe-wrapper-close-button';

        const iframeSelector = document.querySelector('.js-checkout-payment-iframe-wrapper');

        iframe.setAttribute('src', redirectUrl);

        closeButton.innerHTML = '&#x2715';
        closeButton.addEventListener('click', removeIframe);

        iframeWrapper.append(iframe, closeButton);

        if (iframeSelector === null) {
          document.body.appendChild(iframeWrapper);

          iframeURLChange(iframe, (location) => {
            if (location.origin !== window.location.origin) {
              return false;
            }

            const searchParams = new URLSearchParams(location.search);
            const isOrderCompleted = location.href.includes('/orders/completed');
            const data = {
              url: location.pathname
            };

            if (searchParams.has('success') || isOrderCompleted) {
              removeIframe();
            }

            if (isOrderCompleted) {
              window.parent.ReactNativeWebView.postMessage(JSON.stringify(data));
            }
          });
        }
      }
    } else {
      window.location.assign(redirectUrl);
    }
  };

  onCheckoutTabChange = (selectedTab) => {
    const paymentOption = getValue(selectCurrentPaymentOption);

    if (!paymentOption) {
      return;
    }

    if (this.initialized || selectedTab !== 'payment' || paymentOption.slug !== 'masterpass') {
      return;
    }

    this.init();
  };

  onPaymentOptionChange = (paymentOption) => {
    const selectedCheckoutTab = getValue(selectTab);

    if (paymentOption?.payment_type === 'masterpass' && selectedCheckoutTab === 'payment') {
      this.init();
    }
  };

  toggleCardImage = (isVisible) => {
    const cardImageContainer = document.querySelector(this.cardImageContainerSelector);

    cardImageContainer?.toggleAttribute('hidden', !isVisible);
    cardImageContainer?.classList?.toggle('d-none', !isVisible);
  };

  toggleInstallmentsVisibility = () => {
    const emptyView = document.querySelector(this.installmentsEmptyViewSelector);
    const installmentsContainer = document.querySelector(this.installmentsContainerSelector);
    let isVisible = true;

    if (!(emptyView && installmentsContainer)) {
      return;
    }

    if (typeof this.isDirectPurchase === 'undefined' || !this.installments.length) {
      isVisible = false;
    }

    if (this.isDirectPurchase && document.querySelector(this.cardNumberSelector).value.length < 6) {
      isVisible = false;
    }

    if (
      this.isDirectPurchase === false &&
      !Array.from(document.querySelectorAll('.js-mp-card-item')).find((item) => item.classList.contains('-selected'))
    ) {
      isVisible = false;
    }

    emptyView.toggleAttribute('hidden', isVisible);
    installmentsContainer.toggleAttribute('hidden', !isVisible);
    installmentsContainer.classList.toggle('d-none', !isVisible);
  };

  onInstallmentsUpdate = (installments) => {
    this.installments = installments || [];

    this.toggleInstallmentsVisibility();

    if (!this.binNumber) {
      this.toggleCardImage(false);
      return;
    }

    this.toggleCardImage(true);
  };

  onCreditCardNumberChange = (e) => {
    if (e.key && !e.key.match(/^[0-9]$/) && e.key !== 'Backspace') {
      return;
    }

    const cardNumber = document.querySelector(this.cardNumberSelector).value;
    const binNumberLength = getValue(selectBinNumberLength);

    this.toggleInstallmentsVisibility();

    if (cardNumber.length < binNumberLength) {
      this.toggleCardImage(false);
      return;
    }

    const binNumber = cardNumber.substring(0, binNumberLength);

    if (this.binNumber === binNumber) {
      this.toggleCardImage(true);
      return;
    }

    this.binNumber = binNumber;
    store.dispatch(setMasterPassBinNumber(this.binNumber));
  };

  onCreditCardFormSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();
    this.showCheckoutError('');

    if (!e.isTrusted) {
      return;
    }

    if (!Array.from(document.querySelectorAll(this.installmentRadioSelector)).find((radio) => radio.checked)) {
      this.showCheckoutError(this.translation.pleaseSelectInstallment);
      return;
    }

    if (!this.validateAgreementCheckbox()) {
      return;
    }

    if (this.isDirectPurchase && !this.validateCreditCardForm()) {
      return;
    }

    if (
      this.isDirectPurchase === false &&
      !Array.from(document.querySelectorAll('.js-mp-card-item')).find((item) => item.classList.contains('-selected'))
    ) {
      this.showCheckoutError(this.translation.pleaseSelectCard);
      return;
    }

    this.isPaymentReady = true;
    store.dispatch(setPending(true));
    store.dispatch(getMasterPassOrderNo());
  };

  requestToken = async () => {
    const data = await (await fetch('/orders/masterpass-token/')).json();

    this.token = data.token;
    this.referenceNo = data.req_ref_no;
  };

  checkPaymentErrors = () => {
    const urlSearchParams = new URLSearchParams(location.search);
    const page = urlSearchParams.get('page');
    const responseCode = urlSearchParams.get('responseCode');

    if (page === 'MasterpassCompletePage' && responseCode === '4058') {
      this.showModal({
        contentHTML: `<p style="text-align: center; margin: 30px 0 20px 0;">${this.translation.threeDTransactionFailed}</p>`,
        showLogo: true
      });
    }
  };

  checkUser = () => {
    const formTemplate = document.querySelector('.js-mp-check-form-template');
    const form = formTemplate.content.querySelector('form');

    this.fillFormCommonFields(form);
    form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();

    MFS.checkMasterPass($(form), (status, response) => {
      const { accountStatus } = response;
      let shortStatusCode = accountStatus.slice(0, 6);

      if (shortStatusCode == '000000') {
      } else if (shortStatusCode == '011000') {
        this.showLinkModal();
      } else if (shortStatusCode == '011100') {
        this.listCards();
      }
    });
  };

  listCards = () => {
    const phoneNumber = document.querySelector('.js-mp-phone-input').value;
    const cardListWrapper = document.querySelector(this.cardListWrapperSelector);
    const cardList = document.querySelector('.js-mp-card-list');

    if (!cardListWrapper) {
      console.warn(`MasterPass card list wrapper element not found: ${this.cardListWrapperSelector}`);
      return;
    }

    cardList.innerHTML = '';

    MFS.listCards(phoneNumber, this.token, (statusCode, response) => {
      const directPurchaseSwitchButton = document.querySelector('.js-mp-direct-purchase .js-mp-switch-payment-type');

      if (!response.cards || !response.cards?.length) {
        this.isDirectPurchase = true;

        if (directPurchaseSwitchButton) {
          directPurchaseSwitchButton.remove();
        }

        this.switchPaymentType();
        return;
      }

      if (!directPurchaseSwitchButton) {
        document.querySelector('.js-mp-direct-purchase').insertAdjacentHTML(
          'beforeend',
          `
          <a href="#" class="pz-mp-switch-payment-type-btn js-mp-switch-payment-type">
            ${this.translation.payWithMyMasterPassCard}
          </a>
        `
        );
      }

      cardList.innerHTML = response.cards
        .map((card) => {
          const cardType = this.getCreditCardType(card.Value1.substring(0, 6));

          return `
          <li class="pz-mp-card-list__item js-mp-card-item">
            <div class="pz-mp-card-list__item-detail">
              <p class="pz-mp-card-list__item-card-name js-mp-card-name">${card.Name}</p>
              <p class="pz-mp-card-list__item-card-number js-mp-card-number">${card.Value1}</p>
            </div>
            <img src="${window.URLS.static}img/mp_${cardType}.png" class="pz-mp-card-list__item-card-logo" />
            <button class="pz-mp-card-list__item-delete-btn js-mp-card-delete-btn" data-name="${card.Name}">
              <i class="pz-mp-card-list__item-delete-icon ${this.deleteIconClassName}"></i>
            </button>
          </li>
        `;
        })
        .join('');

      this.isDirectPurchase = false;
      this.bindSwitchButtonEvents();
      this.switchPaymentType();

      document.querySelectorAll('.js-mp-card-item').forEach((button) => {
        button.addEventListener('click', this.onCardItemClick);
      });

      document.querySelectorAll('.js-mp-card-delete-btn').forEach((button) => {
        button.addEventListener('click', this.onDeleteButtonClick);
      });
    });
  };

  onSwitchPaymentTypeButtonClick = (e) => {
    e.preventDefault();

    this.isDirectPurchase = e.currentTarget.dataset.type === 'direct';
    this.switchPaymentType();

    document.querySelectorAll('.js-mp-card-item').forEach((cardItem) => {
      cardItem.classList.remove('-selected');
    });
  };

  switchPaymentType = () => {
    if (this.isDirectPurchase) {
      document.querySelector(this.cardFormWrapperSelector).removeAttribute('hidden');
      document.querySelector(this.cardListWrapperSelector).setAttribute('hidden', '');
    } else {
      document.querySelector(this.cardFormWrapperSelector).setAttribute('hidden', '');
      document.querySelector(this.cardListWrapperSelector).removeAttribute('hidden');
    }

    this.toggleInstallmentsVisibility();
    this.binNumber = null;
    this.toggleCardImage(false);
  };

  onCardItemClick = (e) => {
    e.preventDefault();

    const cardItem = e.currentTarget;
    const cardName = cardItem.querySelector('.js-mp-card-name').innerText;
    const cardNumber = cardItem.querySelector('.js-mp-card-number').innerText;
    const binNumber = cardNumber.substring(0, 6);
    const purchaseFormTemplate = document.querySelector('.js-mp-purchase-form-template');
    const purchaseForm = purchaseFormTemplate.content.querySelector('form');

    document.querySelectorAll('.js-mp-card-item').forEach((cardItem) => {
      cardItem.classList.remove('-selected');
    });

    cardItem.classList.add('-selected');
    store.dispatch(setMasterPassBinNumber(binNumber, true));

    purchaseForm.querySelector('[name="cardAliasName"]').value = cardName.trim();
    purchaseForm.querySelector('[name="listAccountName"]').value = cardName.trim();
  };

  onDeleteButtonClick = async (e) => {
    e.stopPropagation();
    e.preventDefault();

    const cardName = e.currentTarget.dataset.name;
    const confirmed = await new Promise((resolve, reject) => {
      this.showModal({
        contentHTML: `
          <p>${this.translation.deleteCardConfirmation}</p>
          <button class="pz-mp-button js-mp-delete-confirm-btn">${this.translation.delete}</button>
        `,
        showLogo: true,
        onClose: () => {
          resolve(false);
        }
      });

      document.querySelector('.js-mp-delete-confirm-btn').addEventListener('click', () => {
        this.closeModal();
        resolve(true);
      });
    });

    if (!confirmed) {
      return;
    }

    const formTemplate = document.querySelector('.js-mp-delete-form-template');
    const form = formTemplate.content.querySelector('form');

    this.fillFormCommonFields(form);
    form.querySelector('[name="accountAliasName"]').value = cardName;
    form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();

    MFS.deleteCard($(form), (statusCode, response) => {
      this.listCards();
    });
  };

  getCreditCardType = (number) => {
    if (/^5[1-5]/.test(number)) {
      return 'mastercard';
    }

    if (/^4/.test(number)) {
      return 'visa';
    }

    if (/^3[47]/.test(number)) {
      return 'amex';
    }

    if (/^(?:9792|65\d{2}|36|2205)/.test(number)) {
      return 'troy';
    }

    return 'other';
  };

  showLinkModal = () => {
    const template = document.querySelector('.js-mp-link-modal-template');
    const templateContent = template.content.cloneNode(true);

    templateContent.querySelector('.js-mp-link-description').innerText = this.translation.linkCards;
    templateContent.querySelector('.js-mp-link-button').innerText = this.translation.use;
    this.showModal({
      contentHTML: templateContent.querySelector('*').innerHTML,
      showLogo: true
    });

    document.querySelector('.js-mp-link-button').addEventListener('click', this.onLinkButtonClick);
  };

  onLinkButtonClick = () => {
    const formTemplate = document.querySelector('.js-mp-link-form-template');
    const form = formTemplate.content.querySelector('form');
    const linkButton = document.querySelector('.js-mp-link-button');

    linkButton.classList.add('-busy');

    this.fillFormCommonFields(form);
    form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();

    this.linkCardToClient({
      form,
      button: linkButton
    });
  };

  startOtpCountdown = () => {
    const date = new Date().getTime();
    const countdownDate = date + this.otpTimeout;
    const countdownElement = document.querySelector('.js-mp-countdown');
    const resendSmsButton = document.querySelector('.js-mp-resend-sms-btn');

    countdownElement.innerText = '';
    resendSmsButton.classList.add('-disabled');
    clearInterval(this.countdownInterval);

    this.countdownInterval = setInterval(() => {
      const now = new Date().getTime();
      const distance = countdownDate - now;

      if (distance < 0) {
        clearInterval(this.countdownInterval);
        resendSmsButton.classList.remove('-disabled');
        countdownElement.innerText = '';
        return;
      }

      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      countdownElement.innerText = `0${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    }, 1000);
  };

  showModal = ({ title = '', contentHTML, iconClassName = '', showLogo = false, onClose = () => {} }) => {
    document.querySelectorAll('.js-mp-modal').forEach((modal) => {
      modal.remove();
    });

    document.body.insertAdjacentHTML(
      'beforeend',
      `
      <div class="js-mp-modal pz-mp-modal">
        <div class="pz-mp-modal__content">
          <header class="pz-mp-modal__header">
            ${showLogo ? `<img class="pz-mp-modal__logo" src="${window.URLS.static}img/mp_masterpass-logo.png" />` : ''}
            <button class="pz-mp-modal__close-btn js-mp-modal-close-button">&#215;</button>
          </header>
          ${iconClassName.length ? `<i class="pz-mp-modal__icon ${iconClassName}"></i>` : ''}
          <h3 class="pz-mp-modal__title js-mp-modal-title">${title}</h3>
          <div class="pz-mp-modal__content-c">${contentHTML}</div>
        </div>
      </div>
    `
    );

    document.querySelector('.js-mp-modal-close-button').addEventListener('click', () => {
      this.closeModal();
      onClose();
    });
  };

  showOtpModal = ({ title, resolve_ = null, reject_ = null }) =>
    new Promise((resolve, reject) => {
      const modalTemplate = document.querySelector('.js-mp-otp-modal-template');
      const templateContent = modalTemplate.content.cloneNode(true);

      this.showModal({
        title,
        contentHTML: templateContent.querySelector('*').innerHTML,
        iconClassName: this.otpModalIconClassName,
        showLogo: true
      });

      const form = document.querySelector('.js-mp-modal form');
      const submitButton = document.querySelector('.js-mp-otp-submit-button');
      const resendSmsButton = document.querySelector('.js-mp-modal .js-mp-resend-sms-btn');

      submitButton.innerHTML = this.translation.confirm;
      resendSmsButton.innerHTML = this.translation.resendSms;
      this.fillFormCommonFields(form);
      form.querySelector('[name="validationCode"]').setAttribute('placeholder', this.translation.confirmationCode);
      form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();
      this.startOtpCountdown();

      resendSmsButton.addEventListener('click', () => {
        const token = MFS.getLastToken();

        if (token) {
          resendSmsButton.classList.add('-busy');

          MFS.resendOtp(token, 'tur', () => {
            this.startOtpCountdown();
            resendSmsButton.classList.remove('-busy');
          });
        }
      });

      form.addEventListener('submit', (e) => {
        e.preventDefault();

        submitButton.classList.add('-busy');

        MFS.validateTransaction($(form), async (statusCode, response) => {
          submitButton.classList.remove('-busy');

          if (['5001', '5008'].includes(response.responseCode)) {
            this.closeModal();

            const { statusCode: statusCode_, response: response_ } = await this.showOtpModal({
              title: this.responseMessages?.[response.responseCode] || response.responseDescription,
              resolve_: resolve,
              reject_: reject
            }).catch(reject_ || reject);

            (resolve_ || resolve)({ statusCode_, response_ });
          } else if (response.responseDescription.length) {
            this.showOtpFormError(response.responseDescription);
          } else {
            (resolve_ || resolve)({ statusCode, response });
          }
        });
      });
    });

  closeModal = () => {
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval);
    }

    document.querySelector('.js-mp-modal').remove();
  };

  showOtpFormError = (errorText) => {
    const errorEl = document.querySelector('.js-mp-otp-form-error');

    errorEl.innerText = errorText;
    errorEl.removeAttribute('hidden');
  };

  showRegisterFormError = (errorText) => {
    const errorEl = document.querySelector('.js-mp-save-card-error');

    errorEl.innerText = errorText;
    errorEl.toggleAttribute('hidden', !errorText.length);
  };

  showCheckoutError = (errorText) => {
    const errorEl = document.querySelector(this.checkoutErrorSelector);

    errorEl.innerText = errorText;
    errorEl.toggleAttribute('hidden', !errorText.length);
  };

  getLanguage = () => {
    if (this.language) {
      return this.language;
    }

    const htmlLanguage = document.querySelector('html').getAttribute('lang');

    if (htmlLanguage === 'tr') {
      return 'tur';
    } else {
      return 'eng';
    }
  };

  clearCardRegisterForm = () => {
    const cardNameInput = document.querySelector('.js-mp-save-card-input');
    const saveCardCheckbox = document.querySelector('.js-mp-save-card-checkbox');

    cardNameInput.value = '';
    saveCardCheckbox.click();
  };

  fillFormCommonFields = (form) => {
    const { user_phone_number: phoneNumber } = getValue(selectPreOrder);

    form.querySelector('[name="token"]').value = this.token;
    form.querySelector('[name="referenceNo"]').value = this.referenceNo;
    form.querySelectorAll('[name="msisdn"], [name="userId"]').forEach((input) => {
      input.value = `9${phoneNumber}`;
    });
  };

  fillFormCreditCardFields = (form) => {
    const cardNumber = document.querySelector(this.cardNumberSelector).value;
    const expirationMonth = `0${document.querySelector(this.cardExpirationMonthSelector).value}`.slice(-2);
    const expirationYear = document.querySelector(this.cardExpirationYearSelector).value.slice(2, 4);
    const cvv = document.querySelector(this.cardCvvSelector).value;

    form.querySelector('[name="rtaPan"]').value = cardNumber;
    form.querySelector('[name="expiryDate"]').value = `${expirationYear}${expirationMonth}`;
    form.querySelector('[name="cvc"]').value = cvv;
  };

  registerCard = async (e) => {
    e.preventDefault();

    const formTemplate = document.querySelector('.js-mp-register-form-template');
    const form = formTemplate.content.querySelector('form');
    const cardName = document.querySelector('.js-mp-save-card-input').value;
    const errorEl = document.querySelector('.js-mp-save-card-error');
    const saveButton = document.querySelector('.js-mp-save-card-button');

    errorEl.setAttribute('hidden', '');

    if (!cardName.length) {
      this.showRegisterFormError(this.translation.pleaseEnterCardName);
      return;
    }

    saveButton.classList.add('-busy');

    this.fillFormCommonFields(form);
    this.fillFormCreditCardFields(form);
    form.querySelector('[name="accountAliasName"]').value = cardName;
    form.querySelector('[name="sendSmsLanguage"]').value = this.getLanguage();

    MFS.register($(form), async (statusCode, response) => {
      saveButton.classList.remove('-busy');

      if (['5001', '5008'].includes(response.responseCode)) {
        const { statusCode: statusCode_, response: response_ } = await this.showOtpModal({
          title: this.responseMessages?.[response.responseCode] || response.responseDescription
        });

        statusCode = statusCode_;
        response = response_;
      }

      if (response.responseCode == '5460') {
        this.linkCardToClient({
          form,
          message: this.translation.merchantVerification
        });
      }

      if (response.responseDescription.length) {
        if (response.responseCode === '5460') {
          return;
        }

        this.showRegisterFormError(this.responseMessages?.[response.responseCode] || response.responseDescription);
        return;
      }

      this.listCards();
      this.closeModal();
      this.clearCardRegisterForm();
    });
  };
}
